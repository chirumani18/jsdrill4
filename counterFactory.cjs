function counterFactory() {
    let counter = 0;
    return {
        increment: function () {
            return counter = counter + 1;
        },
        decrement: function () {
            return counter = counter - 1;
        }
    }
}

module.exports = counterFactory;
