function cacheFunction(cb) {
    if (cb == undefined) {
        throw Error("partial or no arguments are passes");
    } else {
        let cache = {};
        function callAnotherFunction(...input) {
            let args = JSON.stringify(input);
            if (args in cache) {
                return cache[args];
            }
            else {
                cache[args] = cb(...input);
                return cache[args];
            }
        }
        return callAnotherFunction;
    }
}

module.exports = cacheFunction;