let counterFunction = require('../counterFactory.cjs');

let result = counterFunction();
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());

console.log(result.decrement());
console.log(result.decrement());
console.log(result.decrement());
