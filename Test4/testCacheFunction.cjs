let newCacheFunction = require('../cacheFunction.cjs');

function callBack(...input) {
    return input.reduce((total, value) =>
        total + value, 0);
}

let result = newCacheFunction(callBack);
console.log(result(1, 2, 3, 4, 5));
console.log(result(1, 3, 4, 5));
console.log(result(1, 2, 4, 5));
console.log(result(1, 2, 3, 4, 5));
console.log(result(1, 3, 4, 5));
console.log(result());

