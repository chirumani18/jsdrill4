let limitFunction = require('../limitFunctionCallCount.cjs');

function callBack(...args) {
    return "Executed " + " " + args + " " + "times";
}

let result = limitFunction(callBack, 3);
console.log(result(1, 3, 4));
console.log(result());
console.log(result(2, 3));
console.log(result(3));

