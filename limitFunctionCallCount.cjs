function limitFunctionCallCount(cb, n) {
    if (cb == undefined || n == undefined) {
        throw new Error("partial or no arguments passed");
    }
    else {
        let startValue = 0;
        function callAnotherFunction(...args) {
            if (startValue < n) {
                startValue += 1;
                return cb(...args);
            }
            else {
                return null;
            }
        }
        return callAnotherFunction;
    }
}
module.exports = limitFunctionCallCount;

